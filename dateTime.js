/* date and time script for home page */

var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var days = ["Sun.", "Mon.", "Tue.", "Wed.", "Thu.", "Fri.", "Sat."];

function startTime() {
	var today = new Date();
	
	var hr = today.getHours();
	var min = today.getMinutes();
	var month = months[today.getMonth()];
	var date = today.getDate();
	var day = days[today.getDay()];
	
	min = checkTime(min);
	if (isPM(hr)) {
		if (hr != 12) {
			hr = hr - 12;
		}
		document.getElementById('ap').innerHTML = "PM";
	} else {
		if (hr == 0) {
			hr = 12;
		}
		document.getElementById('ap').innerHTML = "AM";
	}
	document.getElementById('time').innerHTML = hr + ":" + min;
	document.getElementById('dt').innerHTML = month + " " + date;
	document.getElementById('day').innerHTML = day;
	var t = setTimeout(startTime, 500);
}

function checkTime(m) {
	if (m < 10) {m = "0" + m};
	return m;
}

function isPM(h) {
	if (h > 11)
		return true;
	else
		return false;
}